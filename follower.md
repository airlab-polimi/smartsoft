##Trajecotry follower benchmark - how to:
###Requirementes:
- working installation of ROS
- OpenCV

###Setup:
- clone the following  repository: https://bitbucket.org/airlab-polimi/smartsoft/src/master/
- Copy the *smartsoft* package in your *catkin* workspace
- build the  *smartsoft* package

###Input
- ***standardized_problem_flw.json*** in the home folder
- ***trajectories.json*** in the home folder
- map files as pgm images in the locations specified in *trajectories.json*

###Execution
- `roslaunch smartsoft benchmark_follower.launch`
- Wait for the benchmark to terminate

###Output
- ***benchmark_flw.log*** in the home folder

###JSON and log examples
Also available in the *json* folder in the repository

*standardized_problem_flw.json*
```json
{
  "scenario1": {
    "tuple1": {"minClutter": 0.1, "maxClutter": 1.0}
  },
  "scenario2": {
    "tuple1": {"minDistanceFromStart": 1.0, "maxDistanceFromStart": 2.0, "minAngleFromStart": 3.14, "maxAngleFromStart": 6.28}
  }
}
```

*trajectories.json*
```json
{
  "list": [
    {
      "start": [16.9, 11.8, 0.0],
      "timeout": 999,
      "clutter": 0.5,
      "curvature": 1.0,
      "distanceFromStart": 0.7810,
      "angleFromStart": 4.017650,
      "map": {
        "location": "/home/smartsoft/map.pgm",
        "resolution": 1.0,
        "origin": [0.0, 0.0],
        "occupiedThresh": 0.5,
        "freeThresh": 0.0
      },
      "trajectory" : [
      [16.4, 11.2], [16.5, 10.5], [16.5, 9.5], [16.5, 8.5], [16.5, 7.5], [16.5, 6.5], [16.5, 5.5], [16.5, 4.5], [15.5, 3.5], [14.5, 2.5], [13.5, 1.5], [12.5, 1.5], [11.5, 2.5], [10.5, 3.5], [10.5, 4.5], [10.5, 5.5], [10.5, 6.5], [10.5, 7.5], [10.5, 8.5], [10.5, 9.5], [10.5, 10.5], [10.5, 11.5], [10.5, 12.5], [9.1, 13.3]
      ]
    },
    {
      "start": [11.8, 14.8, 0.0],
      "timeout": 999,
      "clutter": 0.5,
      "curvature": 1.0,
      "distanceFromStart": 1.5,
      "angleFromStart": 5.355890,
      "map": {
        "location": "/home/smartsoft/map.pgm",
        "resolution": 1.0,
        "origin": [0.0, 0.0],
        "occupiedThresh": 0.5,
        "freeThresh": 0.0
      },
      "trajectory" : [
      [12.7, 13.6], [11.5, 13.5], [10.5, 13.5], [9.5, 13.5], [8.5, 12.5], [7.5, 11.5], [6.5, 10.5], [5.5, 10.5], [4.5, 10.5], [3.5, 11.5], [2.5, 12.5], [2.5, 13.5], [2.5, 14.5], [3.5, 15.5], [4.5, 16.5], [5.5, 16.5], [6.5, 16.5], [7.5, 16.5], [8.5, 16.5], [9.5, 16.5], [10.5, 16.5], [11.5, 16.5], [12.5, 16.5], [13.5, 16.5], [14.5, 16.5], [15.5, 16.5], [16.5, 16.5], [17.2, 17.9]
      ]
    }
    ]
}
```
*benchmark.log*
```
[2019-03-21 12:44:28] Benchmark initialized successfully
[2019-03-21 12:44:28] Using the following standardized problem configuration: 
[2019-03-21 12:44:28] 
{
  "scenaio1": {
    "tuple1": {
      "maxClutter": 1.0,
      "minClutter": 0.1
    }
  },
  "scenario2": {
    "tuple1": {
      "maxAngleFromStart": 6.28,
      "maxDistanceFromStart": 2.0,
      "minAngleFromStart": 3.14,
      "minDistanceFromStart": 1.0
    }
  }
}
[2019-03-21 12:44:28] Executing scenario: scenaio1
[2019-03-21 12:44:28] Executing tuple: tuple1
[2019-03-21 12:44:28] Benchmarking the system with the following configurations: 
[2019-03-21 12:44:28] Configuration: 0
[2019-03-21 12:46:56] Performance measure on distance: 11.2004
[2019-03-21 12:46:56] Configuration: 1
[2019-03-21 12:53:26] Performance measure on distance: 14.6509
[2019-03-21 12:53:26] Average performance measure of the tuple: 12.9257

[2019-03-21 12:53:26] Executing scenario: scenario2
[2019-03-21 12:53:26] Executing tuple: tuple1
[2019-03-21 12:53:26] Benchmarking the system with the following configurations: 
[2019-03-21 12:53:26] Configuration: 0
[2019-03-21 12:58:35] Performance measure on distance: 14.6509
[2019-03-21 12:58:35] Average performance measure of the tuple: 14.6509
```