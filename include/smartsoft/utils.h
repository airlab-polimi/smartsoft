#include "json.hpp"
#include "smartsoft/PlannerQuery.h"
#include "smartsoft/FollowerQuery.h"
#include <chrono>

#ifndef PROJECT_UTILS_H
#define PROJECT_UTILS_H

using json = nlohmann::json;

std::string now() {
    using namespace std::chrono;
    std::time_t tt = system_clock::to_time_t(system_clock::now());
    std::stringstream timestamp;
    timestamp<<"["<<std::put_time(std::localtime(&tt), "%F %T")<<"] ";
    return timestamp.str();

}

namespace smartsoft {
    void to_json(json& j, const PlannerQueryRequest& qr) {
        j = json{{"minPassage", qr.minPassage}, {"maxPassage", qr.maxPassage},
                 {"minClutter", qr.minClutter}, {"maxClutter", qr.maxClutter}};
    }

    void from_json(const json& j, PlannerQueryRequest& qr) {
        if(j.find("minPassage") != j.end())
            j.at("minPassage").get_to(qr.minPassage);
        else
            qr.minPassage = std::numeric_limits<typeof(qr.minPassage)>::min();
        if(j.find("maxPassage") != j.end())
            j.at("maxPassage").get_to(qr.maxPassage);
        else
            qr.maxPassage = std::numeric_limits<typeof(qr.maxPassage)>::max();
        //------------------------------------------------------------------------//
        //------------------------------------------------------------------------//
        if(j.find("minClutter") != j.end())
            j.at("minClutter").get_to(qr.minClutter);
        else
            qr.minClutter = std::numeric_limits<typeof(qr.minClutter)>::min();
        if(j.find("maxClutter") != j.end())
            j.at("maxClutter").get_to(qr.maxClutter);
        else
            qr.maxClutter = std::numeric_limits<typeof(qr.maxClutter)>::max();
    }

    void to_json(json& j, const FollowerQueryRequest& qr) {
        j = json{{"minDistanceFromStart", qr.minDistanceFromStart}, {"maxDistanceFromStart", qr.maxDistanceFromStart},
                 {"minAngleFromStart", qr.minAngleFromStart}, {"maxAngleFromStart", qr.maxAngleFromStart},
                 {"minCurvature", qr.minCurvature}, {"maxCurvature", qr.maxCurvature},
                 {"minClutter", qr.minClutter}, {"maxClutter", qr.maxClutter}};
    }

    void from_json(const json& j, FollowerQueryRequest& qr) {
        if(j.find("minDistanceFromStart") != j.end())
            j.at("minDistanceFromStart").get_to(qr.minDistanceFromStart);
        else
            qr.minDistanceFromStart = std::numeric_limits<typeof(qr.minDistanceFromStart)>::min();
        if(j.find("maxDistanceFromStart") != j.end())
            j.at("maxDistanceFromStart").get_to(qr.maxDistanceFromStart);
        else
            qr.maxDistanceFromStart = std::numeric_limits<typeof(qr.maxDistanceFromStart)>::max();
        //------------------------------------------------------------------------//
        //------------------------------------------------------------------------//
        if(j.find("minAngleFromStart") != j.end())
            j.at("minAngleFromStart").get_to(qr.minAngleFromStart);
        else
            qr.minAngleFromStart = std::numeric_limits<typeof(qr.minAngleFromStart)>::min();
        if(j.find("maxAngleFromStart") != j.end())
            j.at("maxAngleFromStart").get_to(qr.maxAngleFromStart);
        else
            qr.maxAngleFromStart = std::numeric_limits<typeof(qr.maxAngleFromStart)>::max();
        //------------------------------------------------------------------------//
        //------------------------------------------------------------------------//
        if(j.find("minCurvature") != j.end())
            j.at("minCurvature").get_to(qr.minCurvature);
        else
            qr.minCurvature = std::numeric_limits<typeof(qr.minCurvature)>::min();
        if(j.find("maxCurvature") != j.end())
            j.at("maxCurvature").get_to(qr.maxCurvature);
        else
            qr.maxCurvature = std::numeric_limits<typeof(qr.maxCurvature)>::max();
        //------------------------------------------------------------------------//
        //------------------------------------------------------------------------//
        if(j.find("minClutter") != j.end())
            j.at("minClutter").get_to(qr.minClutter);
        else
            qr.minClutter = std::numeric_limits<typeof(qr.minClutter)>::min();
        if(j.find("maxClutter") != j.end())
            j.at("maxClutter").get_to(qr.maxClutter);
        else
            qr.maxClutter = std::numeric_limits<typeof(qr.maxClutter)>::max();
    }
}

#endif //PROJECT_UTILS_H
