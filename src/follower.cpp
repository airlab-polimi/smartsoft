#include "ros/ros.h"
#include "smartsoft/Velocity2D.h"
#include "smartsoft/Trajectory.h"
#include <cmath>

#define MAX_OMEGA 1.5
#define MAX_V 0.8
#define PERIOD 0.1

const double TAU = 2 * std::acos(-1);

struct InternalState {
    smartsoft::Pose2D current_pose;
    std::vector<smartsoft::Pose2D> trajectory;
    smartsoft::Pose2D current_goal;
    double timeout;
    ros::Time begin;

    double x_distance() { return current_goal.x - current_pose.x; }
    double y_distance() { return current_goal.y - current_pose.y; }
} istate;

ros::Timer follow;
ros::Publisher velocity;

void follow_cb(const ros::TimerEvent&) {
    double time = (ros::Time::now() - istate.begin).toSec();
    if(time < istate.timeout * 2) {
        smartsoft::Velocity2D cmd;
        double direction = std::atan2(istate.y_distance(), istate.x_distance());
        double angle = std::abs(istate.current_pose.theta - (direction > 0 ? direction : direction + TAU));
        double distance = std::hypot(istate.x_distance(), istate.y_distance());
        if(angle > 0.1) {
            cmd.w = angle/PERIOD > MAX_OMEGA ? MAX_OMEGA : angle/PERIOD;
        } else if(distance > 0.1) {
            cmd.v = distance/PERIOD > MAX_V ? MAX_V : distance/PERIOD;
        } else {
            if(!istate.trajectory.empty()) {
                istate.current_goal = istate.trajectory.front();
                istate.trajectory.erase(istate.trajectory.begin());
                ROS_INFO_STREAM("Trajectory size: "<<istate.trajectory.size());
            } else {
                ROS_INFO_STREAM("Goal reached");
                follow.stop();
            }
        }
        velocity.publish(cmd);
    } else {
        ROS_ERROR("fails");
    }

}

void pose_cb(const smartsoft::Pose2D::ConstPtr& msg) {
    istate.current_pose = *msg;
}

bool save_trajectory(smartsoft::Trajectory::Request  &req, smartsoft::Trajectory::Response &res) {
    istate.trajectory = req.trajectory;
    istate.current_goal = istate.trajectory.front();
    istate.trajectory.erase(istate.trajectory.begin());
    istate.timeout = req.timeout;
    istate.begin = ros::Time::now();
    ROS_ERROR_STREAM("Trajectory size: "<<istate.trajectory.size());
    follow.start();
    return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "follow");
    ros::NodeHandle n;

    follow = n.createTimer(ros::Duration(PERIOD), follow_cb, false, false);
    ros::ServiceServer service = n.advertiseService("trajectory", save_trajectory);
    ros::Subscriber pose = n.subscribe("/pose", 1, pose_cb);
    velocity = n.advertise<smartsoft::Velocity2D>("/cmd_vel", 1);

    ROS_INFO("Ready to follow");
    ros::spin();

    return 0;
}