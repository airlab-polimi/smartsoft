#include "ros/ros.h"
#include "smartsoft/PlannerQuery.h"
#include "smartsoft/json.hpp"
#include <fstream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

using json = nlohmann::json;
std::string filename;

const std::string home = getenv("HOME");

bool database(smartsoft::PlannerQuery::Request  &req, smartsoft::PlannerQuery::Response &res) {
    if(ros::param::has("/map_locations/list")) {
        ros::param::get("/map_locations/list", filename);
    } else {
        filename = home + "/locations.json";
        ROS_INFO_STREAM("Could not find the map locations, using default: "<<filename);
    }
    std::ifstream file(filename);
    json map_locations;
    file >> map_locations;

    for(const json& j : map_locations["list"]) {
        if (j["clutter"].get<double>() >= req.minClutter && j["clutter"].get<double>() <= req.maxClutter &&
            j["passage"].get<double>() >= req.minPassage && j["passage"].get<double>() <= req.maxPassage) {

            smartsoft::MapLocation ml;
            cv::Mat image = cv::imread(j["map"]["location"], cv::IMREAD_GRAYSCALE);
            ml.map.cellSizeX = image.cols;
            ml.map.cellSizeY = image.rows;

            for (auto it = image.begin<uchar>(); it != image.end<uchar>(); ++it) {
                ml.map.cells.push_back(*it);
            }

            for (const json &start : j["starts"]) {
                smartsoft::Pose2D s;
                s.x = start[0];
                s.y = start[1];
                ml.starts.push_back(s);
            }
            for (const json &goal : j["goals"]) {
                smartsoft::Pose2D g;
                g.x = goal[0];
                g.y = goal[1];
                ml.goals.push_back(g);
            }
            ml.map.resolution = j["map"]["resolution"];
            ml.map.occupiedThresh = j["map"]["occupiedThresh"];
            ml.map.freeThresh = j["map"]["freeThresh"];
            ml.map.origin.x = j["map"]["origin"][0];
            ml.map.origin.y = j["map"]["origin"][1];

            res.mapLocations.push_back(ml);
        }
    }
    return !res.mapLocations.empty();
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "database");
    ros::NodeHandle n;

    ros::ServiceServer service = n.advertiseService("database", database);
    ROS_INFO("Ready to database");

    ros::spin();

    return 0;
}