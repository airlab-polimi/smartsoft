#include "ros/ros.h"
#include "smartsoft/utils.h"
#include <fstream>
#include <cstdlib>
#include "smartsoft/Velocity2D.h"
#include "smartsoft/SimulatorInit.h"
#include "smartsoft/Trajectory.h"
#include "ros/master.h"

ros::Publisher pose_pub, vel_pub;
std::set<std::pair<double, double>> path, trajectory;
ros::Time begin, last_vel;
bool downsample = true;
const std::string home = getenv("HOME");

void pose_cb(const smartsoft::Pose2D::ConstPtr& msg) {
    if(downsample) {
        path.insert(std::make_pair(msg->x, msg->y));
        downsample = !downsample;
    }
    pose_pub.publish(msg);
}

void vel_cb(const smartsoft::Velocity2D::ConstPtr& msg) {
    vel_pub.publish(msg);
    last_vel = ros::Time::now();
}

int main(int argc, char **argv) {

    ros::init(argc, argv, "benchmarkerFollower");
    ros::NodeHandle n;

    std::string filename;
    if(ros::param::has("/standardized_problem")) {
        ros::param::get("/standardized_problem", filename);
    } else {
        ROS_INFO_STREAM("Could not find the standardized problem, using default");
        filename = home + "/standardized_problem_flw.json";
    }
    std::string logger;
    if(ros::param::has("/logger")) {
        ros::param::get("/logger", logger);
    } else {
        ROS_INFO_STREAM("Could not find the logger, using default");
        logger = home + "/benchmark_flw.log";
    }

    ROS_INFO("benchmarking");

    std::ifstream file(filename);
    json std_problem;
    file >> std_problem;

    ros::ServiceClient db_client = n.serviceClient<smartsoft::FollowerQuery>("database");
    ros::ServiceClient sim_client = n.serviceClient<smartsoft::SimulatorInit>("init");
    ros::ServiceClient flw_client = n.serviceClient<smartsoft::Trajectory>("trajectory");
    ros::ServiceClient trj1 = n.serviceClient<smartsoft::Trajectory>("trajectory1");

    pose_pub = n.advertise<smartsoft::Pose2D>("/pose_bm", 1);
    vel_pub = n.advertise<smartsoft::Velocity2D>("/cmd_vel_bm", 1);
    ros::Subscriber pos_sub = n.subscribe("/pose", 1, pose_cb);
    ros::Subscriber vel_sub = n.subscribe("/cmd_vel", 1, vel_cb);

    std::ofstream logfile(logger);
    logfile<<now()<<"Benchmark initialized successfully"<<std::endl;
    logfile<<now()<<"Using the following standardized problem configuration: "<<std::endl;
    logfile<<now()<<std::endl<<std_problem.dump(2)<<std::endl;

    std::vector<std::string> node_list;
    ros::master::getNodes(node_list);

    while(node_list.size() < 4) {
        ros::Duration(1).sleep();
        ros::master::getNodes(node_list);
    }

    for(const auto& scenario : std_problem.items()) {
        logfile<<now()<<"Executing scenario: "<<scenario.key()<<std::endl;
        for(const auto& tuple : scenario.value().items()) {
            logfile<<now()<<"Executing tuple: "<<tuple.key()<<std::endl;
            smartsoft::FollowerQuery qsrv;
            qsrv.request = tuple.value().get<smartsoft::FollowerQueryRequest>();
            if(db_client.call(qsrv)) {
                std::vector<double> performances;
                int j = 0;
                logfile<<now()<<"Benchmarking the system with the following configurations: "<<std::endl;
                for(const smartsoft::LocatedTrajectory& trj : qsrv.response.locatedTrajectories) {
                    logfile<<now()<<"Configuration: "<<j++<<std::endl;

                    smartsoft::SimulatorInit simsrv;
                    simsrv.request.map = trj.map;
                    simsrv.request.initial_pose = trj.start;
                    smartsoft::Trajectory trjsrv;
                    trjsrv.request.trajectory = trj.trajectory;
                    for(const smartsoft::Pose2D& t : trjsrv.request.trajectory) {
                        trajectory.insert(std::make_pair(t.x, t.y));
                    }
                    trjsrv.request.timeout = trj.timeout;

                    if(sim_client.call(simsrv) && trj1.call(trjsrv) && flw_client.call(trjsrv)) {
                        begin = ros::Time::now();
                        last_vel = ros::Time::now();
                        while((ros::Time::now() - begin).toSec() < trjsrv.request.timeout * 2 &&
                        (ros::Time::now() - last_vel).toSec() < 1.0) {
                            ros::spinOnce();
                        }

                        //Hausdorff distance
                        double max_distance_p = 0;
                        for(const std::pair<double, double> p : path) {
                            double min_distance_p = std::numeric_limits<double>::max();
                            for(const std::pair<double, double> t : trajectory) {
                                min_distance_p = std::min(min_distance_p, std::hypot(p.first - t.first, p.second - t.second));
                            }
                            max_distance_p = std::max(max_distance_p, min_distance_p);
                        }

                        double max_distance_t = 0;
                        for(const std::pair<double, double> t : trajectory) {
                            double min_distance_t = std::numeric_limits<double>::max();
                            for(const std::pair<double, double> p : path) {
                                min_distance_t = std::min(min_distance_t, std::hypot(p.first - t.first, p.second - t.second));
                            }
                            max_distance_t = std::max(max_distance_t, min_distance_t);
                        }
                        //-----------------------------//
                        performances.push_back(std::max(max_distance_p, max_distance_t));
                        logfile<<now()<<"Performance measure on distance: "<<std::max(max_distance_p, max_distance_t)<<std::endl;
                    }
                }
                logfile<<now()<<"Average performance measure of the tuple: "<<std::accumulate(performances.begin(), performances.end(), 0.0)/performances.size()<<std::endl;
                logfile<<std::endl;
            }
        }
    }

    return 0;
}