#include "ros/ros.h"
#include "smartsoft/Velocity2D.h"
#include "smartsoft/Trajectory.h"
#include "smartsoft/SimulatorInit.h"
#include <cmath>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#define PERIOD 0.01
#define FACTOR 50


const double TAU = 2 * std::acos(-1);

struct InternalState {
    smartsoft::Pose2D current_pose;
    smartsoft::Velocity2D setpoint;
    smartsoft::Map2D map;
} istate;

ros::Timer simulate;
ros::Publisher pose;
char sim_window[] = "Simulator";
cv::Mat sim_image;

void simulate_cb(const ros::TimerEvent&) {
    istate.current_pose.theta = std::fmod((istate.current_pose.theta + istate.setpoint.w * PERIOD), TAU);
    double distance = istate.setpoint.v * PERIOD;
    istate.current_pose.x = istate.current_pose.x + distance * std::cos(istate.current_pose.theta);
    istate.current_pose.y = istate.current_pose.y + distance * std::sin(istate.current_pose.theta);
    circle(sim_image, cv::Point(int(istate.current_pose.x * FACTOR), int(istate.current_pose.y * FACTOR)), 5, cv::Scalar( 0, 0, 255 ), cv::FILLED, cv::LINE_8);
    imshow(sim_window, sim_image);
    cv::waitKey(PERIOD * 1000);
    pose.publish(istate.current_pose);
}

bool initialize(smartsoft::SimulatorInit::Request  &req, smartsoft::SimulatorInit::Response &res) {
    istate.current_pose = req.initial_pose;
    istate.map = req.map;
    sim_image = cv::Mat::zeros(int(std::ceil(istate.map.cellSizeX * istate.map.resolution * FACTOR)),
            int(std::ceil(istate.map.cellSizeX * istate.map.resolution * FACTOR)),
            CV_8UC3 );
    //TODO works only for a specific combination of coordinate and resolution
    circle(sim_image, cv::Point(int(istate.current_pose.x * FACTOR), int(istate.current_pose.y * FACTOR)),
            5, cv::Scalar( 0, 0, 255 ), cv::FILLED, cv::LINE_8);
    int index = 0;
    for(int y = 0; y < istate.map.cellSizeX; y++){
        for(int x = 0; x < istate.map.cellSizeY; x++) {
            if (((255 - istate.map.cells[index]) / 255.0) < istate.map.occupiedThresh) {
                rectangle(sim_image, cv::Point(x * FACTOR, y * FACTOR), cv::Point((x + 1) * FACTOR, (y + 1) * FACTOR),
                        cv::Scalar( 255, 255, 255 ), cv::FILLED, cv::LINE_8);
            }
            index++;
        }
    }
    cv::imshow(sim_window,sim_image);
    simulate.start();
    return true;
}

void velocity_cb(const smartsoft::Velocity2D::ConstPtr& msg) {
    istate.setpoint = *msg;
    ROS_INFO_STREAM("New setpoint (v, w): "<<istate.setpoint.v<<", "<<istate.setpoint.w);
}

//TODO works only for a specific combination of coordinate and resolution
bool save_trajectory(smartsoft::Trajectory::Request  &req, smartsoft::Trajectory::Response &res) {
    for(auto  it = req.trajectory.begin(); it != req.trajectory.end() - 1; ++it) {
        line(sim_image, cv::Point(int(it->x * FACTOR), int(it->y * FACTOR)),
                cv::Point(int((it+1)->x * FACTOR), int((it+1)->y * FACTOR)), cv::Scalar(255, 0, 0), 3);
    }
    for(const smartsoft::Pose2D& p : req.trajectory) {
        circle(sim_image, cv::Point(int(p.x * FACTOR), int(p.y * FACTOR)), 5, cv::Scalar(255, 0, 0), cv::FILLED, cv::LINE_8);
    }
    return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "simulate");
    ros::NodeHandle n;

    simulate = n.createTimer(ros::Duration(PERIOD), simulate_cb, false, false);
    ros::ServiceServer trj = n.advertiseService("trajectory1", save_trajectory);
    ros::ServiceServer service = n.advertiseService("init", initialize);
    ros::Subscriber velocity = n.subscribe("/cmd_vel", 1, velocity_cb);
    pose = n.advertise<smartsoft::Pose2D>("/pose", 1);

    ROS_INFO("Ready to simulate");
    ros::spin();

    return 0;
}