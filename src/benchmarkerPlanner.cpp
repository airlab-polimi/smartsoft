#include "ros/ros.h"
#include "smartsoft/PlannerQuery.h"
#include "smartsoft/Plan.h"
#include <map>
#include "smartsoft/utils.h"
#include <fstream>
#include <ctime>
#include <numeric>

const std::string home = getenv("HOME");

int main(int argc, char **argv) {

    ros::init(argc, argv, "benchmarkerPlanner");
    ros::NodeHandle n;

    std::string filename;
    if(ros::param::has("/standardized_problem")) {
        ros::param::get("/standardized_problem", filename);
    } else {
        filename = home + "/standardized_problem.json";
        ROS_INFO_STREAM("Could not find the standardized problem, using default: "<<filename);
    }
    std::string logger;
    if(ros::param::has("/logger")) {
        ros::param::get("/logger", logger);
    } else {
        logger = home + "/benchmark.log";
        ROS_INFO_STREAM("Could not find the logger, using default: "<<logger);
    }

    ROS_INFO("benchmarking");

    std::ifstream file(filename);
    json std_problem;
    file >> std_problem;

    ros::ServiceClient db_client = n.serviceClient<smartsoft::PlannerQuery>("database");
    ros::ServiceClient pl_client = n.serviceClient<smartsoft::Plan>("plan");

    std::ofstream logfile(logger);
    logfile<<now()<<"Benchmark initialized successfully"<<std::endl;
    logfile<<now()<<"Using the following standardized problem configuration: "<<std::endl;
    logfile<<now()<<std::endl<<std_problem.dump(2)<<std::endl;


    for(const auto& scenario : std_problem.items()) {
        logfile<<now()<<"Executing scenario: "<<scenario.key()<<std::endl;
        for(const auto& tuple : scenario.value().items()) {
            logfile<<now()<<"Executing tuple: "<<tuple.key()<<std::endl;
            smartsoft::PlannerQuery qsrv;
            qsrv.request = tuple.value().get<smartsoft::PlannerQueryRequest>();
            if(db_client.call(qsrv)) {
                std::vector<double> performances;
                int j = 0;
                logfile<<now()<<"Benchmarking the system with the following configurations: "<<std::endl;
                for(const smartsoft::MapLocation& ml : qsrv.response.mapLocations) {
                    logfile<<now()<<"Configuration: "<<j++<<std::endl;
                    int i = 0;
                    for(const smartsoft::Pose2D& start : ml.starts) {
                        smartsoft::Plan psrv;
                        psrv.request.start = start;
                        psrv.request.goal = ml.goals[i];
                        psrv.request.map = ml.map;
                        double distance = std::hypot((start.x - ml.goals[i].x), (start.y - ml.goals[i].y));
                        i++;
                        double plan_distance = 0;
                        if(pl_client.call(psrv)) {
                            smartsoft::Pose2D previous = psrv.response.trajectory.front();
                            for(auto it = psrv.response.trajectory.begin() + 1; it != psrv.response.trajectory.end(); ++it) {
                                plan_distance = plan_distance + std::hypot((previous.x - it->x),(previous.y - it->y));
                                previous = *it;
                            }
                            logfile<<now()<<"Minimum euclidian distance: "<<distance<<", Planned distance: "<<plan_distance<<std::endl;
                            performances.push_back((plan_distance - distance)/distance);
                            logfile<<now()<<"Performance measure on distance: "<<(plan_distance - distance)/distance<<std::endl;
                        }
                    }
                }
                logfile<<now()<<"Average performance measure of the tuple: "<<std::accumulate(performances.begin(), performances.end(), 0.0)/performances.size()<<std::endl;
                logfile<<std::endl;
            }
        }
    }

    return 0;
}