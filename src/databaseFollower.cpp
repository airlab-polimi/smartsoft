#include "ros/ros.h"
#include "smartsoft/FollowerQuery.h"
#include "smartsoft/json.hpp"
#include <fstream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

using json = nlohmann::json;
std::string filename;

const std::string home = getenv("HOME");

bool database(smartsoft::FollowerQuery::Request  &req, smartsoft::FollowerQuery::Response &res) {
    if(ros::param::has("/located_trajectories/list")) {
        ros::param::get("/located_trajectories/list", filename);
    } else {
        filename = home + "/trajectories.json";
        ROS_INFO_STREAM("Could not find the located trajectories, using default: "<<filename);
    }
    std::ifstream file(filename);
    json map_locations;
    file >> map_locations;

    for(const json& j : map_locations["list"]) {
        if (j["distanceFromStart"].get<double>() >= req.minDistanceFromStart && j["distanceFromStart"].get<double>() <= req.maxDistanceFromStart &&
            j["angleFromStart"].get<double>() >= req.minAngleFromStart && j["angleFromStart"].get<double>() <= req.maxAngleFromStart &&
            j["curvature"].get<double>() >= req.minCurvature && j["curvature"].get<double>() <= req.maxCurvature &&
            j["clutter"].get<double>() >= req.minClutter && j["clutter"].get<double>() <= req.maxClutter) {

            smartsoft::LocatedTrajectory lt;
            cv::Mat image = cv::imread(j["map"]["location"], cv::IMREAD_GRAYSCALE);
            lt.map.cellSizeX = image.cols;
            lt.map.cellSizeY = image.rows;

            for (auto it = image.begin<uchar>(); it != image.end<uchar>(); ++it) {
                lt.map.cells.push_back(*it);
            }

            lt.map.resolution = j["map"]["resolution"];
            lt.map.occupiedThresh = j["map"]["occupiedThresh"];
            lt.map.freeThresh = j["map"]["freeThresh"];
            lt.map.origin.x = j["map"]["origin"][0];
            lt.map.origin.y = j["map"]["origin"][1];

            lt.start.x = j["start"][0];
            lt.start.y = j["start"][1];
            lt.start.theta = j["start"][2];

            lt.timeout = j["timeout"];

            for(const json& p: j["trajectory"]) {
                smartsoft::Pose2D sp;
                sp.x = p[0];
                sp.y = p[1];
                lt.trajectory.push_back(sp);
            }

            res.locatedTrajectories.push_back(lt);
        }
    }
    return !res.locatedTrajectories.empty();
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "database");
    ros::NodeHandle n;

    ros::ServiceServer service = n.advertiseService("database", database);
    ROS_INFO("Ready to database");

    ros::spin();

    return 0;
}