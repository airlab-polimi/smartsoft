#include "ros/ros.h"
#include "smartsoft/Plan.h"
#include <unordered_set>
#include <queue>
#include <functional>
#include <unordered_map>

struct GridLocation {
    int x, y;

    inline bool operator==(const GridLocation& other) const {
        return x == other.x && y == other.y;
    }

    inline bool operator!=(const GridLocation& other) const {
        return x != other.x || y != other.y;
    }

    inline bool operator<(const GridLocation& other) const {
        return x < other.x && y < other.y;
    }

    inline bool operator>(const GridLocation& other) const {
        return x > other.x || y > other.y;
    }

    GridLocation(int x_, int y_) : x(x_), y(y_) {};
    GridLocation() : x(0), y(0) {};
};

namespace std {
/* implement hash function so we can put GridLocation into an unordered_set */
    template <> struct hash<GridLocation> {
        typedef GridLocation argument_type;
        typedef std::size_t result_type;
        std::size_t operator()(const GridLocation& id) const noexcept {
            return std::hash<int>()(id.x ^ (id.y << 4));
        }
    };
}


struct SquareGrid {
    static std::array<GridLocation, 8> DIRS;

    int size_x, size_y;
    double resolution;
    double origin_x, origin_y;
    std::unordered_set<GridLocation> walls;

    SquareGrid(int size_x_, int size_y_)
            : size_x(size_x_), size_y(size_y_) {}

    bool in_bounds(GridLocation id) const {
        return 0 <= id.x && id.x < size_x
               && 0 <= id.y && id.y < size_y;
    }

    bool passable(GridLocation id) const {
        return walls.find(id) == walls.end();
    }

    std::vector<GridLocation> neighbors(GridLocation id) const {
        std::vector<GridLocation> results;

        for (GridLocation dir : DIRS) {
            GridLocation next{id.x + dir.x, id.y + dir.y};
            if (in_bounds(next) && passable(next)) {
                results.push_back(next);
            }
        }

        if ((id.x + id.y) % 2 == 0) {
            // aesthetic improvement on square grids
            std::reverse(results.begin(), results.end());
        }

        return results;
    }

    double cost(GridLocation from_node, GridLocation to_node) const {
        return std::hypot((from_node.x - to_node.x),(from_node.y - to_node.y));
    }

    void mapToWorld(int mx, int my, double& wx, double& wy) {
        wx = origin_x + (mx + 0.5) * resolution;
        wy = origin_y + (my + 0.5) * resolution;
    }

    bool worldToMap(double wx, double wy, int& mx, int& my) {
        if (wx < origin_x || wy < origin_y)
            return false;

        mx = (int)((wx - origin_x) / resolution);
        my = (int)((wy - origin_y) / resolution);

        return (mx < size_x && my < size_y);
    }
};

std::array<GridLocation, 8> SquareGrid::DIRS =
        {GridLocation{1, 0}, GridLocation{0, -1}, GridLocation{-1, 0}, GridLocation{0, 1},
         GridLocation{1, 1}, GridLocation{-1, -1}, GridLocation{-1, 1}, GridLocation{1, -1}};

template<typename T, typename priority_t>
struct PriorityQueue {
    typedef std::pair<T, priority_t> PQElement;
    std::priority_queue<PQElement, std::vector<PQElement>, std::greater<PQElement>> elements;

    inline bool empty() const {
        return elements.empty();
    }

    inline void put(T item, priority_t priority) {
        elements.emplace(item, priority);
    }

    priority_t get() {
        priority_t best_item = elements.top().second;
        elements.pop();
        return best_item;
    }
};

template<typename Location, typename Graph>
void dijkstra_search(Graph graph, Location start, Location goal,
        std::unordered_map<Location, Location>& came_from,
        std::unordered_map<Location, double>& cost_so_far) {
    PriorityQueue<double, Location> frontier;
    frontier.put(0, start);

    ROS_INFO_STREAM("graph size "<<graph.walls.size());
    ROS_INFO_STREAM("frontier size "<<frontier.elements.size());
    ROS_INFO_STREAM("start "<<start.x<<" "<<start.y);
    ROS_INFO_STREAM("goal "<<goal.x<<" "<<goal.y);

    came_from[start] = start;
    cost_so_far[start] = 0;

    while (!frontier.empty()) {
        Location current = frontier.get();

        if (current == goal) {
            break;
        }

        for (Location next : graph.neighbors(current)) {
            double new_cost = cost_so_far[current] + graph.cost(current, next);
            if (cost_so_far.find(next) == cost_so_far.end() || new_cost < cost_so_far[next]) {
                cost_so_far[next] = new_cost;
                came_from[next] = current;
                frontier.put(new_cost, next);
            }
        }
    }
}

template<typename Location>
std::vector<Location> reconstruct_path(Location start, Location goal, std::unordered_map<Location, Location> came_from) {
    std::vector<Location> path;
    Location current = goal;
    while (current != start) {
        path.push_back(current);
        current = came_from[current];
    }
    path.push_back(start); // optional
    std::reverse(path.begin(), path.end());
    return path;
}

void draw_grid(SquareGrid local_map, std::vector<GridLocation> path) {
    char map[local_map.size_x][local_map.size_y];

    for(int x = 0; x < local_map.size_x; x++) {
        for(int y=0; y < local_map.size_y; y++) {
            map[x][y]='.';
        }
    }

    for (const auto& elem : local_map.walls) {
        map[elem.x][elem.y] = '#';
    }

    for (const auto& elem : path) {
        map[elem.x][elem.y] = '@';
    }

    for(int x = 0; x < local_map.size_x; x++) {
        for(int y=0; y < local_map.size_y; y++) {
            std::cout<<map[x][y];
        }
        std::cout<<std::endl;
    }
}


bool plan(smartsoft::Plan::Request  &req, smartsoft::Plan::Response &res) {

    ROS_INFO_STREAM("Received planning request");
    SquareGrid local_map(req.map.cellSizeX, req.map.cellSizeY);
    local_map.resolution = req.map.resolution;
    local_map.origin_x = req.map.origin.x;
    local_map.origin_y = req.map.origin.y;

    ROS_INFO_STREAM("Creating walls");
    int index = 0;
    for(int y = 0; y < req.map.cellSizeX; y++){
        for(int x = 0; x < req.map.cellSizeY; x++) {
            if (((255 - req.map.cells[index]) / 255.0) > req.map.occupiedThresh) {
                local_map.walls.emplace(x,y);
            }
            index++;
        }
    }

    ROS_INFO_STREAM("convert coordinates");
    int sx, sy, gx, gy;
    local_map.worldToMap(req.start.x, req.start.y, sx, sy);
    local_map.worldToMap(req.goal.x, req.goal.y, gx, gy);
    GridLocation start(sx, sy);
    GridLocation goal(gx, gy);

    std::unordered_map<GridLocation, GridLocation> came_from;
    std::unordered_map<GridLocation, double> cost_so_far;

    ROS_INFO_STREAM("plan");
    dijkstra_search(local_map, start, goal, came_from, cost_so_far);
    std::vector<GridLocation> path = reconstruct_path(start, goal, came_from);
    ROS_INFO_STREAM("done");

    for (const auto& elem : path) {
        smartsoft::Pose2D step;
        local_map.mapToWorld(elem.x, elem.y, step.x, step.y);
        res.trajectory.push_back(step);
    }
    res.trajectory.front().x = req.start.x;
    res.trajectory.front().y = req.start.y;
    res.trajectory.back().x = req.goal.x;
    res.trajectory.back().y = req.goal.y;

//    draw_grid(local_map, path);

    return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "plan");
    ros::NodeHandle n;

    ros::ServiceServer service = n.advertiseService("plan", plan);
    ROS_INFO("Ready to plan");
    ros::spin();

    return 0;
}