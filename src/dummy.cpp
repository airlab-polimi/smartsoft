#include "ros/ros.h"
#include "smartsoft/Plan.h"
#include "smartsoft/Trajectory.h"
#include "smartsoft/SimulatorInit.h"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

int main(int argc, char **argv) {
    ros::init(argc, argv, "dummy");
    ros::NodeHandle n;

    ROS_INFO("Ready to dummy");

    smartsoft::Plan p;
    cv::Mat image = cv::imread("/home/gianluca/map.pgm", cv::IMREAD_GRAYSCALE);
    p.request.map.cellSizeX = image.cols;
    p.request.map.cellSizeY = image.rows;

    p.request.map.resolution = 1.0;
    p.request.map.occupiedThresh = 0.5;

    for (auto it = image.begin<uchar>(); it != image.end<uchar>(); ++it) {
        p.request.map.cells.push_back(*it);
    }

//    p.request.start.x = 16.4;
    p.request.start.x = 12.7;
//    p.request.start.y = 11.2;
    p.request.start.y = 13.6;
//    p.request.goal.x = 9.1;
    p.request.goal.x = 17.2;
//    p.request.goal.y = 13.3;
    p.request.goal.y = 17.9;

    ros::ServiceClient pl_client = n.serviceClient<smartsoft::Plan>("plan");
    ros::ServiceClient sim_client = n.serviceClient<smartsoft::SimulatorInit>("init");
    ros::ServiceClient flw_client = n.serviceClient<smartsoft::Trajectory>("trajectory");
    ros::ServiceClient trj1 = n.serviceClient<smartsoft::Trajectory>("trajectory1");

    if(pl_client.call(p)) {
        smartsoft::SimulatorInit sin;
        sin.request.initial_pose.x = 11.8;
        sin.request.initial_pose.y = 14.8;
        sin.request.initial_pose.theta = 0.0;
        sin.request.map = p.request.map;
        sim_client.call(sin);

        smartsoft::Trajectory trj;
        trj.request.trajectory = p.response.trajectory;
        for(const smartsoft::Pose2D& t : trj.request.trajectory) {
            std::cout<<"["<<t.x<<", "<<t.y<<"], ";
        }
        std::cout<<std::endl;
        trj.request.timeout = 999.0;
        trj1.call(trj);
        flw_client.call(trj);
    }

    return 0;
}